import React, { Component } from 'react'
import {View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, ActivityIndicator} from 'react-native'
import { connect } from 'react-redux';
import {fetchAPI} from '../redux/MovieAction';

class MovieScreen extends Component {

    componentDidMount(){
        this.props.fetching('http://www.omdbapi.com/?s=naruto&apikey=997061b4&')
    }

    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.title}>MOVIE w/ REDUX</Text>
                <ScrollView>
                    {
                        this.props.loading ? <ActivityIndicator style={{flex:1}} size={100} color={'#03506f'} /> :
                        this.props.movie.map((isi, i)=>{
                            return(
                                <TouchableOpacity key={i} >
                                    <View style={styles.box}>
                                        <Image source={{ uri: isi.Poster }} style={styles.poster} />
                                        <View style={styles.movAttribute}>
                                            <Text style={styles.MovTitle}> {isi.Title} </Text>
                                            <View style={styles.catYear} >
                                                <Text> {isi.Year} </Text>
                                                <Text> {isi.Type} </Text>
                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            )
                        })
                    }
                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        name: state.MovieReducer.name,
        movie: state.MovieReducer.Film,
        loading: state.MovieReducer.lagiLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        fetching: (url) => dispatch(fetchAPI(url)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieScreen)

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'lightblue'
    },
    box: {
        flexDirection:"row", 
        backgroundColor:'#03506f',
        marginHorizontal:20,
        padding:5,
        borderBottomWidth:15,
        borderBottomRightRadius:15,
        borderStartWidth:10,
        borderTopStartRadius:15,
        borderColor:'#0a043c',
        elevation:10,
        marginBottom:7
    },
    title: {
        alignSelf: 'center',
        marginVertical: 20,
        fontSize:20,
        fontWeight:'bold',
        color:'#03506f'
    },
    movAttribute:{
        marginHorizontal:10, 
        justifyContent:'space-between', 
        marginVertical:10, 
        backgroundColor:"#bbbbbb", 
        flex:1, 
        borderRadius:10, 
        padding:8,
        elevation:8,
        borderWidth:3
    },
    catYear:{
        flexDirection:"row", 
        justifyContent:'space-between', 
        backgroundColor:"#bbbbbb"
    },
    MovTitle:{
        fontSize:18, 
        fontWeight:'bold', 
        alignSelf:'center'
    },
    poster:{
        height: 150, 
        width: 100, 
        margin:10
    }
})