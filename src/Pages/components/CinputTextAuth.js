import React from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

const CinputText = ({Placeholder, borderWidth}) => {
    return (
        <View style={{flexDirection:"row", alignItems:"center",  borderColor: "black", }}>
        <Icon name="key" size={30} color="black" />
            <View style={{
               marginLeft: 5,
                width: "90%", flexDirection: "row", alignItems: "center", justifyContent: "space-between",
                borderWidth:borderWidth,  borderBottomWidth: 2, 
            }}>
                <TextInput style={{color:"black", fontSize:16}} placeholder={Placeholder} secureTextEntry={true} />
                <Icon name="eye-slash" size={20} color="blue" />
            </View>
        </View>
    )
}

export default CinputText;

const styles = StyleSheet.create({})
