import React, {Component} from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {connect} from 'react-redux'

class ListRedux extends Component {
    constructor(){
        super()
    }

    render() {
        return (
            <View>
                <Text style={{fontWeight:'bold', alignSelf:'center', fontSize:20}}>List Data From Redux</Text>
                {this.props.data.map((item,i) => {
                    return (
                        <View key={i} style={styles.container}>
                            <Text style={{width:'25%'}}> {` ${i+1} - ${item.name}`} </Text>
                            <Text style={{width:'25%'}}>{item.address}</Text>
                        </View>
                    )
                })}
            </View>
            
        )
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.reducer.students
    }
}

export default connect(mapStateToProps)(ListRedux)

const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        padding:10
    }
})