import React, {Component} from 'react'
import {View, Text, Button} from 'react-native'
import {connect} from 'react-redux'
import { deleteData } from '../redux/action'
import ReduxBasic2 from './ReduxBasic2'

class ReduxBasic extends Component {
    constructor(){
        super()
    }
    handleDeleteData = (id) => {
        this.props.deleteData(id)
    }

    render(){
        return (
            <View>
                <Text> Title : {this.props.title} </Text>
                <Text> Description : {this.props.description} </Text>
                <Text>-------------------------------------------------------------</Text>
                <Button title='Hapus Data' onPress={() => this.handleDeleteData(2)} />
                <ReduxBasic2/>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        title: state.title,
        description: state.description
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        deleteData : (id) => dispatch(deleteData(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReduxBasic)