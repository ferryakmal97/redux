import React from 'react'
import { View, Text } from 'react-native'
import Avatar from '../child/Avatar'
import LengthChild from '../child/LengthChild'

const ChildComponent1 = () => {
    return (
        <View style={{ marginTop: 20, backgroundColor: "aqua", borderColor: "black", borderWidth: 3, padding: 20 }}>
            <Text style={{alignSelf:"center", color:"black", fontSize:20}}> Child Component</Text>
              <View style={{flexDirection:"row"}}>
                <Avatar />
                <LengthChild color={"green"} width={"70%"} text={"Child"}/>
              </View>
        </View>
    )
}

export default ChildComponent1;
