import AsyncStorage from '@react-native-async-storage/async-storage'
import {createStore, applyMiddleware} from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import thunk from 'redux-thunk';
import combineReducer from './combineReducer';

const persistConfig = {
    key: 'root',
    storage: AsyncStorage
}

const persistedReducer = persistReducer(persistConfig, combineReducer)

const Store = createStore(persistedReducer,applyMiddleware(thunk))
const Persistor = persistStore(Store)

export {Store, Persistor}