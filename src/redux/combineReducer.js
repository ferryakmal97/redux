import reducer from './reducer'
import MovieReducer from './MovieReducer'
import {combineReducers} from 'redux'

const combineReducer = combineReducers ({
    reducer, MovieReducer
})

export default combineReducer