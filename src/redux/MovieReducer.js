const InitialState = {
    Film : [],
    name: 'akmal',
    lagiLoading: true
}

const MovieReducer = (state = InitialState, action) => {
    switch (action.type){
        case 'GET-MOVIE':
            return{
                ...state,
                Film : action.payload,
                lagiLoading : false
            }
        default:
            return state
    }
}

export default MovieReducer