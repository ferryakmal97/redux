import Axios from "axios";

const fetchAPI = (url) => {
    return async function(dispatch) {
        return await Axios.get(url)
          .then((ress) => {
          dispatch(setArticleDetails(ress.data.Search));
        });
      };
}

function setArticleDetails(data) {
    console.log('cek respon :', data)
    return {
      type: 'GET-MOVIE',
      payload: data
    };
}

export {fetchAPI};